#!/usr/bin/env python3

import inkex


def find_groups(element):
    """Find recursively all groups and return them as list."""
    groups = []
    for child in element[:]:
        if child.tag_name == 'g':
            groups.append(child)
            subgroups = find_groups(child)
            groups.extend(subgroups)
    return groups


def ungroup(element):
    """Ungroup a group and do some cleanups"""
    # remove clip path
    if 'clip-path' in element.attrib:
        del element.attrib['clip-path']
    # apply transformation on children
    if element.transform:
        for child in element[:]:
            element.composed_transform(child)
    # move childs to parent if we are not on a layer group
    if element.groupmode != 'layer':
        parent = element.getparent()
        childs = element[:]
        parent.extend(childs)
        parent.remove(element)


def debug(element):
    """print debug output for an element"""
    inkex.utils.errormsg(f"{element.eid}/{element.label} "
                         f"top:{element.bounding_box().top} bottom:{element.bounding_box().bottom} "
                         f"left:{element.bounding_box().left} right:{element.bounding_box().right} )")


class DHLLabel(inkex.Effect):
    def effect(self):
        # remove definitions (get rid of clip paths)
        for element in self.svg[:]:
            if element.tag_name == 'defs':
                element.getparent().remove(element)

        # ungroup everything - keep top level group for layer compatibility in inkscape
        for element in reversed(find_groups(self.svg)):
            ungroup(element)

        # find top layer
        top_layer = None
        for element in self.svg[:]:
            if element.tag_name == 'g' and element.groupmode == 'layer':
                top_layer = element
                break
        assert top_layer is not None

        routing_code_ident_code = inkex.Group.new(label='routing_code_ident_code')
        header = inkex.Group.new(label='header')
        address = inkex.Group.new(label='address')
        security_code = inkex.Group.new(label='security_code')
        footer = inkex.Group.new(label='footer')

        # regroup things, so we can work with it
        for element in top_layer[:]:
            if element.label == 'cut':
                debug(element)

            # remove information part
            if element.bounding_box().top <= 465:
                element.getparent().remove(element)
                continue

            #if element.eid == 'path360':
            #    debug(element)

            # routing code and ident code
            if element.bounding_box().left >= 320:
                routing_code_ident_code.append(element)
                continue

            # Remove advertisement
            if 210 <= element.bounding_box().left <= 232.5:
                element.getparent().remove(element)
                continue

            # Header group
            if element.bounding_box().left < 51:
                header.append(element)
                continue

            # Address group
            if 51 <= element.bounding_box().left <= 210 and element.bounding_box().bottom <= 688.5:
                address.append(element)
                continue

            # Seucrity group
            if 51 <= element.bounding_box().left <= 210 and element.bounding_box().bottom > 688.5:
                security_code.append(element)
                continue

            # Footer group
            if 210 < element.bounding_box().left < 280:
                footer.append(element)
                continue

        # move groups arround
        info = inkex.Group.new(label='info')
        info.append(header)
        info.append(address)
        info.append(security_code)
        info.append(footer)
        top_layer.append(info)
        top_layer.append(routing_code_ident_code)

        #resize info label
        address.transform.add_matrix("matrix(0.92095932,0,0,0.92095932,4.1804869,38.817394)")
        security_code.transform.add_matrix("matrix(1.2038548,0,0,1.2038548,-11.028544,-157.67132)")
        footer.transform.add_translate(-34.500091)

        # move elements around
        info.transform.add_translate(-18, -280)
        routing_code_ident_code.transform.add_translate(-345, 60)

        top_layer.transform.add_scale(0.75, 0.75).add_translate(3, 260)

        # resize svg
        self.svg.set('width', 234.331)
        self.svg.set('height', 680.315)
        self.svg.set('viewBox', '0 0 234.331 680.315)')

if __name__ == '__main__':
    DHLLabel().run()
